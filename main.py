import cherrypy
import os, os.path
class HelloWorld(object):
    
    @cherrypy.expose
    def index(self):
        #replace the return string below with your templating engine of choice
        return """<html>
<head>
        <title>CherryPy static content example</title>
        <link rel="stylesheet" type="text/css" href="/css/styles.css" type="text/css"></link>
</head>
<html>
<body>
<h1>Static example</h1>
<img src="/images/plus.gif">
</body>
</html>"""

cherrypy.quickstart(HelloWorld(), config="conf/app.cfg")